### Assumptions

- There are two versions here:
    - docker-compose.yml
    - k8s-yopass.yml

- Both presumes you're handling SSL upstream in Traefik or via an ingress controller
